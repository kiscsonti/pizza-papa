# Pizza Papa

This is a project that is required to complete our course. 

# How to setup project

https://www.youtube.com/playlist?list=PLYPFxrXyK0Bx9SBkNhJr1e2-NlIq4E7ED

# Hasznos Linkek

UML Class diagram tutorial: https://www.youtube.com/watch?v=UI6lqHOVHic  
UML Sequence diagram tutorial: https://www.youtube.com/watch?v=pCK6prSq8aw  
UML Use case diagram tutorial: https://www.youtube.com/watch?v=zid-MVo7M-E  

Markdown tutorial: https://pandao.github.io/editor.md/en.html