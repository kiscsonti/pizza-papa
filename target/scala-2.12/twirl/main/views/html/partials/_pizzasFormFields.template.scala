
package views.html.partials

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object _pizzasFormFields extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Form[Pizza],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(pizzaForm : Form[Pizza]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),_display_(/*3.2*/()),format.raw/*3.4*/("""

"""),_display_(/*5.2*/helper/*5.8*/.inputText(pizzaForm("id"), 'class -> "form-control")),format.raw/*5.61*/("""
"""),_display_(/*6.2*/helper/*6.8*/.inputText(pizzaForm("name"), 'class -> "form-control")),format.raw/*6.63*/("""
"""),_display_(/*7.2*/helper/*7.8*/.inputText(pizzaForm("price"), 'class -> "form-control")),format.raw/*7.64*/("""
"""),_display_(/*8.2*/helper/*8.8*/.inputText(pizzaForm("toppings"), 'class -> "form-control")))
      }
    }
  }

  def render(pizzaForm:Form[Pizza]): play.twirl.api.HtmlFormat.Appendable = apply(pizzaForm)

  def f:((Form[Pizza]) => play.twirl.api.HtmlFormat.Appendable) = (pizzaForm) => apply(pizzaForm)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Fri Sep 28 19:13:16 CEST 2018
                  SOURCE: /home/stormy/School/rf/play_projekkek/git/pizza-papa/app/views/partials/_pizzasFormFields.scala.html
                  HASH: b470124657178220633e6f677cadd198afe74db6
                  MATRIX: 974->1|1093->27|1120->29|1141->31|1169->34|1182->40|1255->93|1282->95|1295->101|1370->156|1397->158|1410->164|1486->220|1513->222|1526->228
                  LINES: 28->1|33->2|34->3|34->3|36->5|36->5|36->5|37->6|37->6|37->6|38->7|38->7|38->7|39->8|39->8
                  -- GENERATED --
              */
          