
package views.html.errors

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*1.2*/import views.html.Pizzas.layout

object _404 extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](_display_(/*4.2*/layout("404 - Page Not Found")/*4.32*/ {_display_(Seq[Any](format.raw/*4.34*/("""
    """),format.raw/*5.5*/("""<div class="jumbotron">
        <h1>404</h1>
        <h3>Nem találom az oldaltot</h3>
    </div>
""")))}))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Fri Sep 28 22:15:34 CEST 2018
                  SOURCE: /home/stormy/School/rf/play_projekkek/git/pizza-papa/app/views/errors/_404.scala.html
                  HASH: 31fc727f014fcdc051d91179f53111775b27593d
                  MATRIX: 658->1|1075->36|1113->66|1152->68|1183->73
                  LINES: 24->1|34->4|34->4|34->4|35->5
                  -- GENERATED --
              */
          