
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object layout extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template2[String,Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String)(body: Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""<html>
    <head>
        <title>"""),_display_(/*4.17*/title),format.raw/*4.22*/("""</title>
        <link rel="stylesheet" media="screen" href=""""),_display_(/*5.54*/routes/*5.60*/.Assets.versioned("css/bootstrap.min.css")),format.raw/*5.102*/("""">
        <link rel="shortcut icon" type="image/png" href=""""),_display_(/*6.59*/routes/*6.65*/.Assets.versioned("css/boostrap-theme.min.css")),format.raw/*6.112*/("""">
        <link rel="shortcut icon" type="image/png" href=""""),_display_(/*7.59*/routes/*7.65*/.Assets.versioned("css/style.css")),format.raw/*7.99*/("""">
    </head>

    <body>

        <div class="container">
            """),_display_(/*13.14*/body),format.raw/*13.18*/("""
        """),format.raw/*14.9*/("""</div>

        <script src=""""),_display_(/*16.23*/routes/*16.29*/.Assets.versioned("js/jquery-3.3.1.min.js")),format.raw/*16.72*/("""" type="text/javascript"></script>
        <script src=""""),_display_(/*17.23*/routes/*17.29*/.Assets.versioned("js/bootstrap.min.js")),format.raw/*17.69*/("""" type="text/javascript"></script>
        <script src=""""),_display_(/*18.23*/routes/*18.29*/.Assets.versioned("js/custom.js")),format.raw/*18.62*/("""" type="text/javascript"></script>
    </body>
</html>"""))
      }
    }
  }

  def render(title:String,body:Html): play.twirl.api.HtmlFormat.Appendable = apply(title)(body)

  def f:((String) => (Html) => play.twirl.api.HtmlFormat.Appendable) = (title) => (body) => apply(title)(body)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Wed Oct 03 21:31:56 CEST 2018
                  SOURCE: /home/stormy/School/rf/play_projekkek/git/pizza-papa/Projekt/pizza-papa/app/views/layout.scala.html
                  HASH: b264945d96fe58cba4f9a4aaaf8d69a6f272bfb6
                  MATRIX: 954->1|1075->29|1135->63|1160->68|1248->130|1262->136|1325->178|1412->239|1426->245|1494->292|1581->353|1595->359|1649->393|1749->466|1774->470|1810->479|1867->509|1882->515|1946->558|2030->615|2045->621|2106->661|2190->718|2205->724|2259->757
                  LINES: 28->1|33->2|35->4|35->4|36->5|36->5|36->5|37->6|37->6|37->6|38->7|38->7|38->7|44->13|44->13|45->14|47->16|47->16|47->16|48->17|48->17|48->17|49->18|49->18|49->18
                  -- GENERATED --
              */
          