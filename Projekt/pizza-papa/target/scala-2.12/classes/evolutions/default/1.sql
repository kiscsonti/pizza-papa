# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table user (
  id                            integer not null,
  email                         varchar(255),
  username                      varchar(255),
  password                      varchar(255),
  admin                         integer(1),
  toppings                      varchar(255),
  constraint pk_user primary key (id)
);


# --- !Downs

drop table if exists user;

